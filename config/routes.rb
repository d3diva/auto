Rails.application.routes.draw do

  resources :shirts do
    get :autocomplete_brand_name, on: :collection
  end
  resources :brands
  resources :products
  resources :categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
