FROM ruby:2.5.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /auto
WORKDIR /auto

ADD Gemfile /auto/Gemfile
ADD Gemfile.lock /auto/Gemfile.lock

RUN bundle install

ADD . /auto
