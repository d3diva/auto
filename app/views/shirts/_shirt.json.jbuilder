json.extract! shirt, :id, :name, :price, :brand_id, :created_at, :updated_at
json.url shirt_url(shirt, format: :json)
