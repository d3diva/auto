class CreateShirts < ActiveRecord::Migration[5.1]
  def change
    create_table :shirts do |t|
      t.string :name
      t.decimal :price
      t.references :brand, foreign_key: true

      t.timestamps
    end
  end
end
